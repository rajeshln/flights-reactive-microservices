package server.flight;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import server.airline.FlightFactory;
import server.common.Utils;
import server.common.model.Airport;
import server.common.model.ExchangeRate;
import server.common.model.Flight;
import server.common.model.FlightRequest;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;
import static server.common.Constants.EndPoint.*;

@SpringBootTest(classes = FlightApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@AutoConfigureMockMvc
public class FlightControllerTests {
    @Autowired
    WebTestClient webTestClient;

    @MockBean
    private FlightService service;

    @Test
    public void testFindFlights() {
        Flight expected = FlightFactory.randomFlight();
        FlightRequest flightRequest = FlightFactory.buildRequestFrom(expected);

        when(service.findFlights(
                flightRequest.getDepartureAirport(),
                flightRequest.getDepartureDate(),
                flightRequest.getArrivalAirport(),
                flightRequest.getCurrency()
        )).thenReturn(Flux.just(expected));

        List<Flight> result = webTestClient
                .get()
                .uri("/" + FLIGHTS +
                     "?departureAirport=" + flightRequest.getDepartureAirport() +
                     "&departureDate=" + Utils.dateString(flightRequest.getDepartureDate()) +
                     "&arrivalAirport=" + flightRequest.getArrivalAirport() +
                     "&currency=" + flightRequest.getCurrency())
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Flight.class)
                .returnResult()
                .getResponseBody();

        assertThat(result).isEqualTo(List.of(expected));

        verify(service, times(1)).findFlights(
                flightRequest.getDepartureAirport(),
                flightRequest.getDepartureDate(),
                flightRequest.getArrivalAirport(),
                flightRequest.getCurrency()
        );
        clearInvocations(service);
    }

    @Test
    public void testFindBestPrice() {
        Flight expected = FlightFactory.randomFlight();
        FlightRequest flightRequest = FlightFactory.buildRequestFrom(expected);

        when(service.findBestPriceFlights(
                flightRequest.getDepartureAirport(),
                flightRequest.getDepartureDate(),
                flightRequest.getArrivalAirport(),
                flightRequest.getCurrency()
        )).thenReturn(Flux.just(expected));

        List<Flight> result = webTestClient
                .get()
                .uri("/" + BEST_PRICE_FLIGHTS +
                                "?departureAirport=" + flightRequest.getDepartureAirport() +
                                "&departureDate=" + Utils.dateString(flightRequest.getDepartureDate()) +
                                "&arrivalAirport=" + flightRequest.getArrivalAirport() +
                                "&currency=" + flightRequest.getCurrency())
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Flight.class)
                .returnResult()
                .getResponseBody();

        assertThat(result).isEqualTo(List.of(expected));

        verify(service, times(1)).findBestPriceFlights(
                flightRequest.getDepartureAirport(),
                flightRequest.getDepartureDate(),
                flightRequest.getArrivalAirport(),
                flightRequest.getCurrency()
        );

        clearInvocations(service);
    }

    @Test
    public void testFindFlightDates() {
        List<LocalDate> expected = new ArrayList<>(2);
        expected.add(LocalDate.parse("1234-01-01"));
        expected.add(LocalDate.parse("1234-01-02"));
        String departureAirport = "from here";
        String arrivalAirport = "to there";

        when(service.findDepartureDates(departureAirport, arrivalAirport))
                .thenReturn(Flux.fromIterable(expected));

        assertThat(service.findDepartureDates(departureAirport, arrivalAirport)).isNotNull();

        List<LocalDate> result = webTestClient
                .get()
                .uri("/" + FLIGHT_DATES +
                     "?departureAirport=" + departureAirport +
                     "&arrivalAirport=" + arrivalAirport)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(LocalDate.class)
                .returnResult()
                .getResponseBody();

        verify(service, times(2)).findDepartureDates(departureAirport, arrivalAirport);

        assertThat(result).isEqualTo(expected);
    }

    @Test
    public void testGetAirports() {
        Airport[] expected = new Airport[]{
                new Airport("ABC", "ABC description"),
                new Airport("DEF", "DEF description"),
                new Airport("HIJ", "HIJ description")
        };
        Flux<Airport> flux = Flux.fromArray(expected);

        when(service.getAirports()).thenReturn(flux);

        List<Airport> result = webTestClient.get()
                .uri("/" + AIRPORTS)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Airport.class)
                .returnResult()
                .getResponseBody();

        verify(service, times(1)).getAirports();

        assertThat(result).isEqualTo(Arrays.asList(expected));
    }

    @Test
    public void testGetExchangeRate() {
        ExchangeRate expected = ExchangeRate.of("ABC", "DEF", 0.7);

        when(service.getRate(
                expected.getFromCurrency(),
                expected.getToCurrency())
        ).thenReturn(Mono.just(expected));

        ExchangeRate result = webTestClient.get()
                .uri("/" + RATE +
                     "?fromCurrency=" + expected.getFromCurrency() +
                     "&toCurrency=" + expected.getToCurrency())
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectBody(ExchangeRate.class)
                .returnResult()
                .getResponseBody();

        verify(service, times(1)).getRate(expected.getFromCurrency(), expected.getToCurrency());

        assertThat(result).isEqualTo(expected);
    }
}