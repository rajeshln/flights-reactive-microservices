package server.airport;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.r2dbc.DataR2dbcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import reactor.core.publisher.Flux;
import server.common.model.Airport;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@DataR2dbcTest
public class AirportServiceTests {
    @MockBean
    AirportRepository repository;

    @Autowired
    AirportService service;

    @Test
    public void testGetAirports() {
        Airport[] airports = new Airport[]{
                new Airport("ABC", "ABC description"),
                new Airport("DEF", "DEF description"),
                new Airport("HIJ", "HIJ description")
        };

        Flux<Airport> expected = Flux.fromArray(airports);

        when(repository.findAll()).thenReturn(expected);

        List<Airport> result = service.getAirports().collectList().block();

        assertThat(result).isEqualTo(Arrays.asList(airports));
    }
}