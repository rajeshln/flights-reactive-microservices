package server.airline.common;

import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import server.common.model.Flight;

import java.time.LocalDate;

/**
 * A persistent repository that contains information about Airline
 * {@link Airport} objects and can be used to store and retrieve
 * airport information from a database asynchronously.
 *
 * The {@code @Repository} annotation indicates that this class
 * provides the mechanism for storage, retrieval, search, update and
 * delete operation on {@link Airport} objects.
 */
@Repository
public interface AirlineRepository
       extends ReactiveCrudRepository<Flight, Long> {
    /**
     * Return a Flux that emits all {@link Flight} objects matching
     * the params.
     *
     * @param departureAirport The departure airport
     * @param departureDate    The departure date
     * @param arrivalAirport   The arrival airport
     * @return A {@link Flux} that emits all {@link Flight} objects
     * that match the parameters
     */
    Flux<Flight> findByDepartureAirportAndDepartureDateAndArrivalAirport
        (@Param("departureAirport") String departureAirport,
         @Param("departureDate") LocalDate departureDate,
         @Param("arrivalAirport") String arrivalAirport);

    /**
     * Return a {@link Flux} that emits all distinct {@link Flight}
     * departure dates for flights between the specified {@code
     * departureAirport} and {@code arrivalAirport}.
     *
     * @param departureAirport The departure airport
     * @param arrivalAirport   The arrival airport
     * @return A {@link Flux} that emits {@link LocalDate} objects
     * that match the query
     */
    @Query("SELECT DISTINCT departure_date FROM FLIGHT " +
            "WHERE departure_airport = :departureAirport " +
            "AND arrival_airport = :arrivalAirport " +
            "ORDER BY departure_date ASC;")
    Flux<LocalDate> findDepartureDates
        (@Param("departureAirport") String departureAirport,
         @Param("arrivalAirport") String arrivalAirport);
}
