package server.airline.swa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;

import server.airline.common.AirlineRepository;
import server.common.Components;
import server.common.Constants;
import server.common.DatabaseInitializer;
import server.common.model.Flight;
import server.common.model.FlightRequest;

/**
 * This class provides the entry point for the Southwest Airlines
 * (SWA) microservice, which defines methods that asynchronously find
 * all available flights, find the best price for a flight request,
 * and find departure dates for a given pair of airports.
 *
 * The {@code @SpringBootApplication} annotation enables apps to use
 * auto-configuration, component scan, and to define extra
 * configurations on their "application" class.  
 * 
 * The {@code @EnableDiscoveryClient} annotation enables service
 * registration and discovery, i.e., this process registers itself
 * with the discovery-server service using its application name.
 * 
 * The {@code @EnableR2dbcRepositories} annotation activates reactive
 * relational repositories using R2DBC.
 *
 * The {@code @EntityScan} annotation is used when entity classes are
 * not placed in the main application package or its sub-packages.
 * 
 * The {@code @ComponentScan} annotation tells Spring the packages to
 * scan for annotated components (i.e., tagged with @Component).
 *
 * The {@code @PropertySources} annotation is used to provide
 * properties files to Spring Environment.
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableR2dbcRepositories(basePackageClasses = AirlineRepository.class)
@EntityScan(basePackageClasses = {FlightRequest.class, Flight.class})
@ComponentScan(basePackageClasses = {
        AirlineRepository.class,
        SWAApplication.class,
        Components.class,
        DatabaseInitializer.class})
@PropertySources({
        @PropertySource(Constants.Resources.EUREKA_CLIENT_PROPERTIES),
        @PropertySource(Constants.Resources.DATABASE_PROPERTIES),
        @PropertySource("classpath:/airline/swa/swa.properties")})
public class SWAApplication {
    /**
     * A static main() entry point is needed to run the SWA
     * microservice.
     */
    public static void main(String[] args) {
        // Launch this microservice via Spring WebFlux.
        SpringApplication.run(SWAApplication.class, args);
    }
}
