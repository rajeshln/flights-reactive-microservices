package server.exchange;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;

import server.common.Constants;
import server.common.DatabaseInitializer;
import server.common.model.ExchangeRate;

/**
 * This class provides the entry point for the currency exchange
 * microservice, which finds and returns the current exchange rate for
 * currencies asynchronously.
 *
 * The {@code @SpringBootApplication} annotation enables apps to use
 * auto-configuration, component scan, and to define extra
 * configurations on their "application" class.
 *
 * The {@code @EnableDiscoveryClient} annotation enables service
 * registration and discovery, i.e., this process registers itself
 * with the discovery-server service using its application name. 
 * 
 * The {@code @EnableR2dbcRepositories} annotation activates reactive
 * relational repositories using R2DBC.
 *
 * The {@code @EntityScan} annotation is used when entity classes are
 * not placed in the main application package or its sub-packages. The
 * @ComponentScan annotation tells Spring the packages to scan for
 * annotated components (i.e., tagged with {@code @Component}). 
 *
 * The {@code @PropertySources} annotation is used to provide
 * properties files to Spring Environment.
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableR2dbcRepositories(basePackageClasses = ExchangeRepository.class)
@EntityScan(basePackageClasses = ExchangeRate.class)
@ComponentScan(basePackageClasses = {ExchangeApplication.class, DatabaseInitializer.class})
@PropertySources({
        @PropertySource(Constants.Resources.EUREKA_CLIENT_PROPERTIES),
        @PropertySource(Constants.Resources.DATABASE_PROPERTIES),
        @PropertySource("classpath:/exchange/exchange.properties")})
public class ExchangeApplication {
    /**
     * A static main() entry point is needed to run the Airports
     * microservice.
     */
    public static void main(String[] args) {
        // Launch this microservice within Spring WebFlux.
        SpringApplication.run(ExchangeApplication.class, args);
    }
}
