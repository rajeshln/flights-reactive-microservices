package server.exchange;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import server.common.model.ExchangeRate;

import static server.common.Constants.EndPoint.RATE;
import static server.common.Constants.EndPoint.RATES;

/**
 * This Spring controller demonstrates how Spring MVC can be used to
 * handle HTTP GET requests via Java reactive programming.  These
 * requests are mapped to endpoint methods that forward to the {@link
 * ExchangeService}, which find and return the current exchange rate
 * for known currencies asynchronously.
 *
 * In Spring's approach to building RESTful web services, HTTP
 * requests are handled by a controller (identified by the
 * {@code @RestController} annotation) that defines the endpoints (aka
 * routes) for each supported operation, i.e., {@code @GetMapping},
 * {@code @PostMapping}, {@code @PutMapping}, and {@code 
 * @DeleteMapping}, which correspond to the HTTP GET, POST, PUT, and
 * DELETE calls, respectively.
 *
 * Spring uses the {@code @GetMapping} annotation to map HTTP GET
 * requests onto methods in the {@link ExchangeController}.  GET
 * requests invoked from any HTTP web client (e.g., a web browser or
 * Android app) or command-line utility (e.g., Curl or Postman).
 *
 * The {@code @CrossOrigin} annotation marks the annotated method or
 * type as permitting cross origin requests, which is required for
 * Eureka redirection.
 */
@RestController
@CrossOrigin("*") // Required for Eureka redirection
public class ExchangeController {
    /**
     * This auto-wired field connects the {@link ExchangeController}
     * to the {@link ExchangeService}.
     */
    @Autowired
    ExchangeService service;

    /**
     * Request used by Eureka Control panel (for debugging only).
     *
     * @return Print something useful (e.g., the class name)
     */
    @GetMapping("/actuator/info")
    String info() {
        return getClass().getName();
    }

    /**
     * Find and return the exchange rate that matches the {@code
     * fromCurrency} and {@code toCurrency} query parameters
     * asynchronously.
     *
     * @param fromCurrency The 3 letter currency code to convert from
     * @param toCurrency   The 3 letter currency code to convert to
     * @return A {@link Mono} that emits the exchange rate between the
     * {@code fromCurrency} and {@code toCurrency} parameters
     */
    @GetMapping(RATE)
    private Mono<ExchangeRate> getRate(@RequestParam String fromCurrency,
                                       @RequestParam String toCurrency) {
        // Return a Mono that emits the exchange rate between the
        // fromCurrency and toCurrency parameters.
        return service
            // Forward to the ExchangeService.
            .getRate(fromCurrency, toCurrency);
    }

    /**
     * Returns a Flux that emits all rates for converting from all
     * know currencies to the provided {@code toCurrency} parameter
     * asynchronously.
     *
     * @param toCurrency The 3 letter currency code to convert from
     * @return A {@link Flux} that emits a list of {@link
     * ExchangeRate} objects that contain the conversion rates from
     * all currencies to the provided {@code toCurrency} parameter
     */
    @GetMapping(RATES)
    private Flux<ExchangeRate> getRates(@RequestParam String toCurrency) {
        // Return a Flux that emits a list of all exchange rates.
        return service.getRates(toCurrency);
    }
}
