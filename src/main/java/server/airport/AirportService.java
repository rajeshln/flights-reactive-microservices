package server.airport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import reactor.core.publisher.Flux;
import server.common.model.Airport;

/**
 * This class defines an implementation method that is called by the
 * {@link AirportController}.  This method asynchronously returns a
 * {@link Flux} that emits all known {@link Airport} objects.
 * 
 * It is annotated as a Spring {@code @Service}, which enables the
 * auto-detection and wiring of dependent implementation classes via
 * classpath scanning.
 */
@Service
public class AirportService {
    /**
     * This auto-wired field connects the {@link AirportService} to
     * the {@link AirportRepository} that stores airline information
     * persistently.
     */
    @Autowired
    AirportRepository repository;

    /**
     * Returns a {@link Flux} that emits all supported {@link Airport}
     * objects asynchronously.
     *
     * @return A {@link Flux} that emits all supported {@link Airport}
     * objects asynchronously
     */
    public Flux<Airport> getAirports() {
        return repository
            // Forward to the persistent repository.
            .findAll();
    }
}
