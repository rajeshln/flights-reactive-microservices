package server.common;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * This class contains Beans that can be injected into classes using
 * the {@code @Autowired} annotation.
 */
@Component
public class Components {
    /**
     * This factory method returns a new {@link WebClient.Builder()}.
     *
     * @return A new {@link WebClient.Builder()}
     */
    @Bean
    @LoadBalanced
    WebClient.Builder builder() {
        return WebClient.builder();
    }

    /**
     * This factory method returns an initialized {@link WebClient}.
     *
     * @param builder A builder used to create a new {@link WebClient}
     * @return An initialized {@link WebClient}
     */
    @Bean
    WebClient webClient(WebClient.Builder builder) {
        return builder.build();
    }

    @Bean
    @LoadBalanced
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }
}
