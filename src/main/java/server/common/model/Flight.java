package server.common.model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.springframework.data.annotation.Id;

/**
 * This "Plain Old Java Object" (POJO) class defines a response for a
 * flight, which is returned by airline microservices in response to
 * flight request query parameters.
 *
 * The {@code @Data} annotation generates all the boilerplate that is
 * normally associated with simple POJOs (Plain Old Java Objects) and
 * getters for all fields, setters for all non-final fields, and
 * appropriate toString, equals and hashCode implementations that
 * involve the fields of the class, and a constructor that initializes
 * all final fields, as well as all non-final fields with no
 * initializer that have been marked with @NonNull, to ensure the
 * field is never null.
 *
 * The {@code @AllArgsConstructor} annotation generates an all-args
 * constructor that includes one argument for every field in the
 * class.
 *
 * The {@code @NoArgsConstructor} will generate a constructor with no
 * parameter.
 *
 * The {@code @Builder} annotation automatically creates a static
 * builder factory method for this class that can be used as follows:
 *
 * Flight flight = Flight
 *   .builder()
 *   .departureAirport("JFK")
 *   .arrivalAirport("BWI")
 *   ...
 *   .build();
 *
 * The {@code @With} annotation generates a method that constructs a
 * clone of the object, but with a new value for this one field.
 *
 * The {@code @Entity} annotation specifies that this class is an
 * entity and is mapped to a database table.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor(force = true)
@Builder
@With
@Entity
public class Flight {
    /**
     * The {@code @Id} annotation indicates the {@code id} field below
     * is the primary key of an {@link Flight} object.  The
     * {@code @GeneratedValue} annotation configures the specified
     * field to auto-increment.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    /**
     * The name of the departure airport.
     */
    String departureAirport;

    /**
     * The date of the departure.  The {@code @JsonFormat} annotation
     * specifies how to format fields and/or properties for JSON
     * output.
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    LocalDate departureDate;

    /**
     * The time of the departure.
     */
    LocalTime departureTime;

    /**
     * The name of the arrival airport.
     */
    String arrivalAirport;

    /**
     * The date of the arrival.  The {@code @JsonFormat} annotation
     * specifies how to format fields and/or properties for JSON
     * output.
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    LocalDate arrivalDate;

    /**
     * The time of the arrival.
     */
    LocalTime arrivalTime;

    /**
     * The distance in kilometers from the departure to the arrival
     * airports.
     */
    int kilometers;

    /**
     * The price of the flight.  The {@code @Column} annotation
     * customizes the mapping between the entity attribute and the
     * database column, which in this case defines the scale and
     * precision of a decimal price.
     */
    @Column(precision = 10, scale = 2)
    double price;

    /**
     * The currency of the price.  The {@code @Column} annotation
     * customizes the mapping between the entity attribute and the
     * database column, which in this case defines the length of
     * String-valued database column to be 3 characters.
     */
    @Column(length = 3)
    String currency;

    /**
     * The IAOC 3 character airline code.  The {@code @Column}
     * annotation customizes the mapping between the entity attribute
     * and the database column, which in this case defines the length
     * of String-valued database column to be 3 characters.
     */
    @Column(length = 3)
    String airlineCode;

    /**
     * Used to create sample data.
     * @return SQL insert header string.
     */
    public static String insertIntoTableHeader() {
        return "insert into FLIGHT (\n" +
               "\tid,\n" +
               "\tdeparture_airport,\n" +
               "\tdeparture_date,\n" +
               "\tdeparture_time,\n" +
               "\tarrival_airport,\n" +
               "\tarrival_date,\n" +
               "\tarrival_time,\n" +
               "\tkilometers,\n" +
               "\tprice,\n" +
               "\tcurrency,\n" +
               "\tairline_code\n" +
               "\tcapacity\n" +
               ") values\n";
    }

    /**
     * Used to create sample data.
     * @return SQL insert value entry string.
     */
    public String insertTableValueEntry() {
        return "(default" +
               ", '" + departureAirport + "'" +
               ", '" + departureDate + "'" +
               ", '" + departureTime + "'" +
               ", '" + arrivalAirport + "'" +
               ", '" + arrivalDate + "'" +
               ", '" + arrivalTime + "'" +
               ", " + kilometers +
               ", " + price +
               ", '" + currency + "'" +
               ", '" + airlineCode + "'" +
               ')';
    }

    /**
     * Creates an SQL insert table definition that can be used to
     * pre-load the FLIGHT database table.
     *
     * @param flights List {@link Flight} objects.
     * @return A complete table insert definition.
     */
    public static String toSqlInsertString(List<Flight> flights) {
        StringBuilder builder =
                new StringBuilder(Flight.insertIntoTableHeader());

        for (int i = 0; i < flights.size(); i++) {
            builder.append("\t");
            builder.append(flights.get(i).insertTableValueEntry());
            builder.append(i < flights.size() - 1 ? ",\n" : ";");
        }

        return builder.toString();
    }
}
